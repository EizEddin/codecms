# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.29)
# Database: codecms
# Generation Time: 2014-06-12 08:59:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cc_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_categories`;

CREATE TABLE `cc_categories` (
  `cat_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table cc_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_posts`;

CREATE TABLE `cc_posts` (
  `post_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `author` varchar(128) NOT NULL DEFAULT 'user',
  `content` text,
  `status` varchar(128) NOT NULL DEFAULT 'unpublished',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_cat` int(11) unsigned DEFAULT NULL,
  `position` int(11) DEFAULT '0',  
  `date_add` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`),
  KEY `users_id` (`users_id`),
  KEY `post_parent` (`post_parent`),
  KEY `post_cat` (`post_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_posts` WRITE;
/*!40000 ALTER TABLE `cc_posts` DISABLE KEYS */;

INSERT INTO `cc_posts` (`post_id`, `users_id`, `title`, `slug`, `author`, `content`, `status`, `date_add`, `modified`, `post_type`, `post_parent`, `post_cat`)
VALUES
	(1,1,'My First Blog Post','my-first-blog-post','John Doe','<p>This is my first blog post!!!</p>\n','published','2013-01-01 12:00:00','2014-05-30 18:05:28','post',0,0),
	(3,1,'Blog','blog','John Doe','<p>Blog</p>','published','2013-01-01 12:00:00','2014-06-12 15:34:47','page',0,0);

/*!40000 ALTER TABLE `cc_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cc_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_settings`;

CREATE TABLE `cc_settings` (
  `settings_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `settings_name` varchar(128) NOT NULL DEFAULT '',
  `settings_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`settings_id`),
  UNIQUE KEY `settings_id` (`settings_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_settings` WRITE;
/*!40000 ALTER TABLE `cc_settings` DISABLE KEYS */;

INSERT INTO `cc_settings` (`settings_id`, `settings_name`, `settings_value`)
VALUES
	(1,'POST_PAGE_CHOSEN','0'),
	(2,'POST_PER_PAGE','10'),
	(3,'ARRANGE_POST_BY','post_id'),
	(4,'ORDER_POST_BY','asc'),
	(5,'DEFAULT_EMAIL','mobius19@live.com'),
	(6,'APP_NAME','CodeCMS');

/*!40000 ALTER TABLE `cc_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cc_user_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_user_sessions`;

CREATE TABLE `cc_user_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_user_sessions` WRITE;
/*!40000 ALTER TABLE `cc_user_sessions` DISABLE KEYS */;

INSERT INTO `cc_user_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('a99ef3cc8d1a901611a952f62b28ff6c','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36',1402563459,'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"email\";s:15:\"admin@admin.com\";s:12:\"is_logged_in\";i:1;s:4:\"role\";s:5:\"admin\";}');

/*!40000 ALTER TABLE `cc_user_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cc_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cc_users`;

CREATE TABLE `cc_users` (
  `users_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identity` varchar(128) NOT NULL DEFAULT '0',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `role` varchar(255) NOT NULL DEFAULT 'subscriber',
  `first_name` varchar(255) DEFAULT '',
  `last_name` varchar(255) DEFAULT '',
  `about` text,
  `last_login` timestamp NULL DEFAULT NULL,
  `is_logged_in` int(11) unsigned DEFAULT '0',
  `pw_recovery` varchar(255) DEFAULT '0',
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cc_users` WRITE;
/*!40000 ALTER TABLE `cc_users` DISABLE KEYS */;

INSERT INTO `cc_users` (`users_id`, `identity`, `username`, `email`, `password`, `role`, `first_name`, `last_name`, `about`, `last_login`, `is_logged_in`, `pw_recovery`)
VALUES
	(1,'0','admin','admin@admin.com','88ea39439e74fa27c09a4fc0bc8ebe6d00978392','admin','John','Doe','<p>about me!</p>\n','0000-00-00 00:00:00',0,'0');

/*!40000 ALTER TABLE `cc_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
