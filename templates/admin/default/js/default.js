function click_toggle(btn, target, btnText, btnTextOrig)
{
	var click = false;
	$(btn).on('click', function(){
		if (click == true) {
			click = false;
			$(target).slideUp();
			$(this).html(btnText).removeClass('btn-info').addClass('btn-warning');
		}else{
			click = true;
			$(target).slideDown();
			$(this).html(btnTextOrig).removeClass('btn-warning').addClass('btn-info');
		};

		return false;
	});	
}

$(document).ready(function(){

	//SETTINGS PAGE
	$('#change_post_page').click(function(){
		$('#choose_page').removeClass('hidden');
		$(this).parent().parent().addClass('hidden');
		$('#POST_PAGE_CHOSEN_label').attr('name', 'post_page');
		$('#choose_page').find("#POST_PAGE_CHOSEN").attr('name', 'POST_PAGE_CHOSEN');
	});

	$('#cancel_edit').click(function(){
		$('#choose_page').addClass('hidden');
		$('#choose_page_default').removeClass('hidden');		
		$('#POST_PAGE_CHOSEN_label').attr('name', 'POST_PAGE_CHOSEN');
		$('#choose_page').find("#POST_PAGE_CHOSEN").attr('name', 'post_page');
	});
	//END SETTINGS PAGE

    $('#select_all').click(function () {
    	var delete_selection = $('tbody').find('.delete_selection');
    	$(delete_selection).attr('checked', this.checked);
    });

    //GET SELECTED POSTS/PAGES FOR DELETION	
	$("#delete_selection").click(function(event) {

		if(!confirm('Delete selected posts?')) return false; //ask user if they're sure
	 
	  	/* stop form from submitting normally */
	  	event.preventDefault();

	  	var page = $("body").attr("id");
	  	var url  = $('#delete_selection').data('url');

  		$.each($('input[name="delete_selection"]:checked'), function() {
			$.ajax({
			  type: "POST",
			  url: url,
			  data: { selected: $(this).val() },
			  success: function(data){
				setTimeout(function () {
					window.location.href = window.location.href;
				}, 1000);

				$('#ajax_message').show().html('Successfully deleted.');
			  },
			  error: function(data) {
			  	console.log(data);
			  },		  
			});
		});	
	});

	//SORTABLE FUNCTION
	$( ".table > tbody" ).sortable({
		handle : '.icon-handle',
		update : function (){

 			var order 	= $(this).sortable('serialize');
 			var url 	= $('#sortable').data('link') + "/sort?";
			
			/*AFTER THE UPDATE CHANGE THE POSITION IN THE DATABASE*/
			$.ajax({
			  type: "GET",
			  url: url + order,
			  data : order,
			  success: function(data){
			  	$('#ajax-response').fadeIn(function(){
			  		$(this).html(data);
			  	}).delay(3000).fadeOut('fast');
			  },
			  error: function(data) {
			  	console.log(data);
			  	$('#ajax-response').removeClass('alert-success').addClass('alert-danger').fadeIn(function(){
			  		$(this).html('Sorry, we have encountered an error. Report it to the cute guy! :P');
			  	}).delay(5000).fadeOut('fast');			  	
			  },		  
			});
		}
	});	
 });