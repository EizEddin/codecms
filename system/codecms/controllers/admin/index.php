<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Index extends Admin_Controller {

    public $table = 'users';

    function __construct(){

        parent::__construct();
    }

	public function index()
    {
        if ( (bool)$this->session->userdata('is_logged_in') === true ) :
            redirect('admin/dashboard');
        else:
            $this->login();
        endif;

	}

    public function dashboard() 
    {
        if ( (bool)$this->session->userdata('is_logged_in') === true ) :
            
            $data['logged_info']        = $this->users_model->logged_in();
            
            $this->template->title      = 'Dashboard';
            $this->template->content->view('templates/admin/default/dashboard', $data);
            $this->template->publish();            
        else:
            $this->login();
        endif;
    }    

	public function login()
    {
        //CREATES VALIDATION OF THE USER INPUT ON THE LOGIN FORM
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|callback_check_details');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|sha1');

        if ( $this->form_validation->run() ) :

            //CHECK IF THIS USER EXIST IN OUR SYSTEM
            if ( $this->users_model->user_exist($this->input->post('email')) !== NULL ) :

                //CHECK EMAIL / PASSWORD COMBO
                $user = $this->db_calls_model->query_post("users", "email", $this->input->post("email"));

                if ($user->password == $this->input->post('password')) :

                    $role = $this->users_model->checkRole();

                    //CHECK IF THE LOGGING IN USER IS AN ADMIN
                    if ( $role == 'admin' ) :

                        //FLASH USERDATA TO BE USED DURING LOGIN
                        $data = array(
                            'email'         => $this->input->post('email'),
                            'is_logged_in'  => 1,
                            'role'          => $role
                        );
                    else:
                        //FLASH USERDATA TO BE USED DURING LOGIN
                        $data = array(
                            'email'         => $this->input->post('email'),
                            'is_logged_in'  => 1,
                            'role'          => $role
                        );
                    endif;

                    $identity               = $this->session->set_userdata($data);

                    //SET LOGGED-IN IN THE DATABASE
                    $params = array(
                        'identity'      => sha1($identity),
                        'is_logged_in'  => 1,
                        'last_login'    => date('Y-m-d H:i:s'),
                    );

                    $this->db_calls_model->update($this->table, 'email', $this->input->post('email'), $params);                    


                    $data['success'] = $this->session->set_flashdata('success', 'Congrats! You have been successfully logged in!');
                    redirect('admin', $data);
                else:
                    $data['error'] = $this->session->set_flashdata('error', 'Sorry, email and password combination does not match.');
                    redirect('admin');
                endif;
            else:
                $data['error'] = $this->session->set_flashdata('error', 'Sorry, we cannot recognize you. Please try again.');
                redirect('admin');
            endif;            
        else:
            
            $this->template->title = 'Login';
            $this->template->set_template('templates/admin/default/login');
            $this->template->content->view('templates/admin/default/loginContent');
            $this->template->publish();
        endif;
	}

    public function logout( $data = ''){

        $this->users_model->logout();
        
        //SET LOGGED-IN IN THE DATABASE
        $params = array(
            'identity'      => 0,
            'is_logged_in'  => 0,
            'last_login'    => date('Y-m-d H:i:s'),
        );

        $this->db_calls_model->update($this->table, 'email', $this->input->post('email'), $params);        
        redirect('admin/login');
    }

    /* -------- FORGOT PASSWORD PAGE -------------------- */
    public function password(){

        //CREATES VALIDATION OF THE USER INPUT ON THE LOGIN FORM
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

        if ( $this->form_validation->run() ) :

            if ( $this->users_model->user_exist( $this->input->post('email') ) === true ) :

                $key = sha1($this->input->post('email') . date("U") );

                $this->users_model->generateKey($this->input->post('email'), $key);

                //SENT THE KEY TOGETHER WITH THE EMAIL
                $this->load->library('email', array('mailtype' => 'html'));
                
                $message = "You have requested for password recovery. <a href='". base_url("get_password/$key") ."'>Click here</a> to create a new password.";
                $this->email->from($this->settings_model->get_setting('DEFAULT_EMAIL'), 'Admin');
                $this->email->to($this->input->post('email'));
                $this->email->subject('Password Recovery');
                $this->email->message($message);
                
                if ( $this->email->send() ) :

                    $data['success'] = $this->session->set_flashdata('success', 'We have sent an email. Pleas check!');
                    redirect('admin', $data);
                else:

                    $data['error'] = $this->session->set_flashdata('error', 'Sorry, the seems to have problem sending the email. Please try again.');
                    redirect('admin', $data);
                endif;
            else :
                
                $data['error'] = $this->session->set_flashdata('error', 'Sorry, we cannot recognize your identity. Please try again.');
                redirect('password');
            endif;
        endif;

        $this->template->title = 'Forgot Password';
        $this->template->set_template('templates/admin/default/password');
        $this->template->content->view('templates/admin/default/passwordContent');
        $this->template->publish();        
    }

    /* CHECK FOR THE CORRECT KEY MATCH OF THE KEY SENT TO THE USERS EMAIL WITH OUR RECORDS KEY */
    public function get_password()
    {

        if ( $this->users_model->check_valid( $this->uri->segment(2) ) || $this->users_model->check_valid( $this->input->post('key') ) ) :
            
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|sha1');

            if ( $this->form_validation->run() ) :

                //SEND AN EMAIL AFTER SUCCESSFUL CHANGING OF PASSWORD
                $user = $this->db_calls_model->query_post('users', 'pw_recovery', $this->input->post('key'));
                
                $this->email->from($this->settings_model->get_setting('DEFAULT_EMAIL'), 'Admin');
                $this->email->to($user->email);

                $this->email->subject('New Password Generatated');
                $this->email->message("You have successfully generated a new password.");

                $this->users_model->new_password();

                $data['success'] = $this->session->set_flashdata('success', 'You have generated a new password! Please login now.');                
                redirect('admin', $data);
            endif;
        else :
            
            $data['error'] = $this->session->set_flashdata('error', 'Sorry, we cannot recognize your identity. Please check again your email.');                
            redirect('password', $data);
        endif;

        $this->template->set_template('templates/admin/default/login');
        $this->template->title = 'Create New Password';
        $this->template->content->view('templates/admin/default/users/new_password');
        $this->template->publish();                    
    }

    //ADDITIONAL CHECKING FOR THE ROLE CALLBACK
    public function check_default($post_string){

      return $post_string == '0' ? FALSE : TRUE;
    }

    public function delete_account(){

        $this->users_model->delete_account();
        redirect('admin');
    }

    public function settings()
    {
        $this->form_validation->set_rules('POST_PAGE_CHOSEN','Show posts in ','trim|xss_clean');
        $this->form_validation->set_rules('POST_PER_PAGE','Show per page','trim|xss_clean|is_natural');
        $this->form_validation->set_rules('ARRANGE_POST_BY','Arrange posts by','trim|xss_clean');
        $this->form_validation->set_rules('ORDER_POST_BY','Order posts by','trim|xss_clean');        

        if ( $this->form_validation->run()) :

            $this->posts_model->update_post_settings();              

            $data['success']    = $this->session->set_flashdata('success', 'You have successfully cofigured your post settings.');
            redirect('admin/settings', $data);
        endif;

        $this->template->title      = 'Settings page';
                
        $data['logged_info']        = $this->users_model->logged_in();

        $params                     = array('post_type' => 'page');
        $data['pages']              = $this->db_calls_model->query_post_array('posts', $params);
        
        $data['post_settings']      = $this->settings_model->settings();
        $data['POST_PAGE_CHOSEN']   = $this->settings_model->get_setting('POST_PAGE_CHOSEN');

        $this->template->content->view('templates/admin/default/settings', $data);        

        $this->template->publish();        
    }
}