<section id="main_content" class="single">
	<?php if ( isset($page) ) : ?>
		<h2>
			<?php echo $page->title; ?>
			<span><?php echo '<span class="author">' . $page->author . "</span> on "; $newDate = date("M jS Y", strtotime($page->date_add)); echo $newDate; ?></span>
		</h2>
		<div class="content"><?php echo $page->content; ?></div>
	<?php else: ?>
		<p class="text-error">Sorry, there are no page to show or page is not existing. </p>
	<?php endif; ?>
</section>