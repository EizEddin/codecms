<section id="main_content" class="posts">
	<?php if ( isset($posts) && !empty($posts) ) : ?>
		<?php foreach ($posts as $post ) : $slug = $post["slug"]; ?>
			<article>
				<h2>
					<a href="<?php echo base_url("view/$slug"); ?>"><?php echo $post['title']; ?></a>
					<span><?php echo '<span class="author">' . $post['author'] . "</span> on "; $newDate = date("M jS Y", strtotime($post['date_add'])); echo $newDate; ?></span>
				</h2>
				<div class="content"><?php echo $post['content']; ?></div>				
			</article>
		<?php endforeach; ?>
	<?php else: ?>
		<p class="text-error">Sorry, there are no page to show or page is not existing. </p>
	<?php endif; ?>
</section>