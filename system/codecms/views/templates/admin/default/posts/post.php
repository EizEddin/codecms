<div class="row clearfix:after posts_list">
	
	<div class="span3 bs-docs-sidebar"><?php $this->load->view('templates/admin/default/shared/sidebar'); ?></div>

	<div class="span9">
		<section id="pages">
			<div class="controlgroup">
				
				<div id="ajax_message" class="text-success alert-block alert-success fade in" style="display:none"></div>
				
				<a class="btn btn-primary" href="<?php echo base_url('admin/posts/create'); ?>">New Post</a>
				<?php 

				//POST LIST FORM
				if ( is_array($posts)) : ?>
				<p class="alert alert-success" style="display:none" id="ajax-response"></p>
				<table class="table table-striped" id="sortable" data-link="<?php echo base_url($this->uri->segment(1) ."/". $this->uri->segment(2)); ?>">
					<thead>
						<tr>
							<th><input type="checkbox" id="select_all" /></th>
							<th class="tbl_title">Title</th>
							<th class="tbl_content hidden-phone">Content</th>
							<th class="tbl_author hidden-phone">Author</th>
							<?php if ( $logged_info['role'] == 'admin' ) : ?><th class="tbl_actions">Actions</th><?php endif; ?>
							<th>Position</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($posts as $post) : $post_url = $post["slug"]; $postID = $post["post_id"]; ?>			
							<tr id="row_<?php echo $post["post_id"]; ?>">
								<td><input class="delete_selection" type="checkbox" name="delete_selection" value="<?php echo $post['post_id']; ?>" /> </td>
								<td class="tbl_title">
									<a href="<?php echo base_url("admin/posts/edit/$postID") ?>"><?php echo $post['title']; ?></a>
								</td>
								<td class="tbl_content hidden-phone"><?php echo word_limiter($post['content'], 10); ?></td>
								<td class="tbl_author hidden-phone"><?php echo $post['author']; ?></td>
								<td class="tbl_actions">
								
								<!-- ACTION TO VIEW THE PAGE-->
								<?php echo anchor( base_url("view/$post_url"), '<i class="icon-search icon-white">&nbsp;</i>', 'target="_blank" class="btn btn-small btn-info "'); ?>
								
								<?php if ( $logged_info['role'] == 'admin' ) : ?>

								<a class="btn btn-small btn-primary" href="<?php echo base_url("admin/posts/edit/$postID") ?>"><i class="icon-pencil icon-white"></i></a>
								<?php echo anchor("admin/posts/delete/$postID", '<i class="icon-trash icon-white"> &nbsp; </i>', 'class="btn btn-small btn-danger"'); ?>
								<?php echo form_open('admin/posts/quick_update'); if ( $post['status'] == 'unpublished') : ?>
									
									<!-- ACTION TO QUICK PUBLISH THE PAGE-->
									<input type="hidden" name="post_id" value="<?php echo $post["post_id"]; ?>" />
									<input type="hidden" name="post_type" value="<?php echo $post["post_type"]; ?>" />
									<button name="status" class="btn btn-danger" value="published" onClick="return confirm('Publish this page?')">
									<i class="icon-remove icon-white icon_status">&nbsp;</i>

								<?php else: ?>

									<!-- ACTION TO QUICK UNPUBLISH THE PAGE-->
									<input type="hidden" name="post_id" value="<?php echo $post["post_id"]; ?>" />
									<input type="hidden" name="post_type" value="<?php echo $post["post_type"]; ?>" />
									<button name="status" class="btn btn-success" value="unpublished" onClick="return confirm('Unpublish this page?')">
									<i class="icon-ok icon-white icon_status">&nbsp;</i>
																			
								<?php endif; echo form_close(); ?>
								<?php endif; ?>
								</td>
								<td><i class="icon-drag icon-handle"></i></td>
							</tr>			
						<?php endforeach; ?>
					</tbody>
				</table>				
				
				<div class="pull-left action_left">
					<button id="delete_selection" name="delete_selection" class="btn btn-danger btn-small" value=""><i class="icon-trash icon-white"> &nbsp; </i> Delete Selected</button>
				</div>

				<div class="pull-right action_right">
					<?php echo $links; //PAGINATION ?>
				</div>
				
				<?php else: ?>

				  <div class="text-error alert-block alert-error fade in">
				    <p>Ooops, No more posts!</p>
				  </div>

				<?php endif; ?>
			</div>
		</section>
	</div>	
</div>