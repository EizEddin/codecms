<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
	<title><?php echo $this->template->title->default("Page Not Found"); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo $this->template->description; ?>">
	<meta name="author" content="">
	<?php echo $this->template->meta; ?>
	<?php echo $this->template->stylesheet; ?>

    <style>
      body { padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */ }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/ico/favicon.ico'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/ico/apple-touch-icon-144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/ico/apple-touch-icon-114-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/ico/apple-touch-icon-72-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/ico/apple-touch-icon-57-precomposed.png'); ?>">
  </head>

  <body id="<?php echo $this->uri->segment(2) ?>" class="dashboard admin">

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" target="_blank" href="<?php echo base_url(); ?>">Code CMS</a>
          <div class="nav-collapse collapse">
            <ul class="nav">            
              <li <?php if ( $this->uri->segment(3) == 'dashboard'): echo "class='active'"; endif; ?>><a href="<?php echo base_url('admin'); ?>">Dashboard</a></li>

              <?php if ( $logged_info['role'] == 'admin') : ?>
              
              <!-- USER MANAGEMENT-->
              <li class="dropdown <?php if ( $this->uri->segment(2) == 'users' ): echo "active"; endif; ?>">
                <a href="<?php echo base_url('admin/users'); ?>">Manage Users</a>
              </li>

              <!-- PAGE MANAGEMENT-->
              <li class="dropdown <?php if ( $this->uri->segment(2) == 'pages' ): echo "active"; endif; ?>">
                <a href="<?php echo base_url('admin/pages'); ?>">Manage Pages</a>
              </li>

              <!-- POST MANAGEMENT-->
              <li class="dropdown <?php if ( $this->uri->segment(2) == 'posts' ) : echo "active"; endif; ?>">
                <a href="<?php echo base_url('admin/posts'); ?>">Manage Post</a>
              </li>
            
              <!-- SETTINGS -->
              <li <?php if ( $this->uri->segment(3) == 'settings'): echo "class='active'"; endif; ?>><a href="<?php echo base_url('admin/settings'); ?>">Settings</a> </li>

            <?php endif; ?>              

            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
    
    <?php if ( validation_errors() ) :  ?>
      <div class="alert alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <?php echo validation_errors(); ?> 
      </div>
    <?php endif;  ?>
            
    <?php if ( $this->session->flashdata('success') ) : ?>
      <div class="alert alert-success fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <?php echo $this->session->flashdata('success'); ?> 
      </div>
    <?php elseif ( $this->session->flashdata('error') ): ?>
      <div class="alert alert-error fade in"> 
        <a class="close" data-dismiss="alert">&times;</a>
        <?php echo $this->session->flashdata('error'); ?> 
      </div>
    <?php endif; ?>
    
    <?php $users_id = $logged_info['users_id']; ?>

    <div id="user_detail" class="input-block-level">      
      <div class="btn-group pull-right">
        <a class="btn btn-primary" href="<?php echo base_url('admin/profile'); ?>"><i class="icon-user icon-white"></i> <?php echo $logged_info['first_name'] .' '. $logged_info['last_name']  ?></a>
        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url('admin/profile'); ?>"><i class="icon-star"></i> View Profile</a></li>
          <li><a href="<?php echo base_url("admin/profile/update/$users_id"); ?>"><i class="icon-pencil"></i> Edit Profile</a></li>
          <li><a href="<?php echo base_url("admin/logout"); ?>"><i class="icon-off"></i> Log Out</a></li>
          <li class="divider"></li>
          <li><?php echo form_open('admin/delete_account', 'id="delete_my_account"'); ?>
            <button class="btn btn-danger bt-small input-block-level" name="delete_account" id="delete_account" value="<?php echo $logged_info['users_id']; ?>" onClick="return confirm('Are you sure you want to delete your acccount?')"> <i class="icon-remove icon-white">&nbsp;</i> Delete Account</button>
              <?php echo form_close(); ?>            
        </ul>
      </div>      
    </div>