<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] 						= 	"index";
$route['404_override'] 								= 	'';

$route['admin'] 									= 	'admin/index/dashboard';
$route['admin/dashboard'] 							= 	'admin/index/dashboard';
$route['admin/logout']			   					= 	'admin/index/logout';
$route['admin/login']			   					= 	'admin/index/login';

//ADMIN USERS
$route['admin/users'] 								= 	'admin/users/index';
$route['admin/users/create'] 						= 	'admin/users/create';
$route['admin/users/update/(:num)'] 				= 	'admin/users/update';
$route['admin/users/update']		 				= 	'admin/users/update';
$route['admin/users/delete/(:num)']		 			= 	'admin/users/delete';

//ADMIN PROFILE
$route['admin/profile'] 							= 	'admin/profile/index';
$route['admin/profile/update']		 				= 	'admin/profile/update';
$route['admin/profile/update/(:num)'] 				= 	'admin/profile/update';

//ADMIN DELETE ACCOUNT
$route['admin/delete_account'] 						= 	'admin/index/delete_account';

//ADMIN PAGES
$route['admin/pages']								=	'admin/pages/index';
$route['admin/pages/create']						=	'admin/pages/create';
$route['admin/pages/edit']							=	'admin/pages/edit';
$route['admin/pages/edit/(:num)']					=	'admin/pages/edit';
$route['admin/pages/(:num)']						=	'admin/pages/index';
$route['admin/pages/quick_update']					=	'admin/pages/quick_update';
$route['admin/pages/delete/(:num)']					=	'admin/pages/delete';
$route['admin/pages/sort']							=	'admin/pages/sort';

//ADMIN POSTS
$route['admin/posts']								=	'admin/posts/index';
$route['admin/posts/create']						=	'admin/posts/create';
$route['admin/posts/edit']							=	'admin/posts/edit';
$route['admin/posts/edit/(:num)']					=	'admin/posts/edit';
$route['admin/posts/(:num)']						=	'admin/posts/index';
$route['admin/posts/quick_update']					=	'admin/posts/quick_update';
$route['admin/posts/delete/(:num)']					=	'admin/posts/delete';
$route['admin/posts/sort']							=	'admin/posts/sort';

$route['admin/settings']							=	'admin/index/settings';
$route['admin/pages/delete_selection']				=	'admin/pages/delete_selection';

//PUBLIC URLS
$route['contact']									=	'contact/index';
$route['contact/submit']							=	'contact/submit';

//PUBLIC PAGES
$route['(:any)']									= 	'page/view';

//PUBLIC FUNCTIONS
$route['insert_new_pw']								=	'admin/index/insert_new_pw';
$route['password'] 									= 	'admin/index/password';
$route['new_password'] 								= 	'admin/index/get_password';
$route['get_password/(:any)'] 						= 	'admin/index/get_password';

/* End of file routes.php */
/* Location: ./application/config/routes.php */